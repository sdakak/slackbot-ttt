import sys
from flask import jsonify, request

import middleware
from Board import Board
from calculate_stats import score_top

"""
This class is responsible for receiving all the slack commands and calling the internal APIs in return.
Separation of Concerns they call it.

"""
# populate active games from DB. This is used to ensure only one active game per channel is allowed
active_games = dict()
all_games = middleware.all_games(False)
for Game in all_games:
  if Game.state == 'ongoing':
    active_games[Game.channel] = Game

"""
Controller method for receiving /ttt command
"""
def slack_bot():
  print 'printing cache'
  for k, v in active_games.iteritems():
    print str(k), ": ", str(v)

  # 10 games occupy 1MB. So with 500MB we can support 5000 games.
  # This dyno has 1GB. So allowing for 500MB overhead we should only cache 5000 games
  # TODO LRU Cache to evict old games
  if len(active_games) > 5000:
    return 'Too many games are being played. Please try later'

  try:
    message, channel, user1, user, board_size = parse_request()
  except:
    return 'unrecognized command'

  active = is_game_active(channel=channel)

  if message == 'topscore':
    return top_score()
  elif message == 'help':
    return help()
  if message.startswith('challenge'):
    if active[0]:
      return "Cant start a new game while a current one is on"
    return setup_game(message=message, channel=channel, user1=user1, user2=user,
                      board_size=board_size)
  elif not active[0]:
    return active[1]
  elif message.isdigit():
    return process_move(message=message, channel=channel, user=user)
  elif message == 'show':
    return show_game(channel=channel)
  else:
    return 'Unrecognized command. Type /ttt help for help'


"""
Is there an active game in this channel
"""
def is_game_active(channel):
  global active_games
  if not active_games.has_key(channel):
    return False, jsonify({
      "response_type": "in_channel",
      "text": "Sorry no active game in this channel.",
      "attachments": [
        {
          "text": "Start a new game with /ttt challenge @another_user"
        }
      ]
    })
  return True, 'Is Active'


"""
Parse the request that slack sends to our API
"""
def parse_request():
  message = str(request.form['text'])
  channel = str(request.form['channel_name'])
  user1 = None
  board_size = 3

  if message.startswith('challenge'):
    user1 = message.split(' ')[1].split('|')[1][:-1]
    try:
      board_size = int(message.split(' ')[2])
    except:
      board_size = 3

  user = str(request.form['user_name'])
  return message, channel, user1, user, board_size


"""
Show if there is a currently active game in the channel
"""
def show_game(channel):
  game = active_games.get(channel)
  board = Board(game)

  return jsonify({
    "response_type": "in_channel",
    "text": "```" + board.to_ascii() + "```",
    "attachments": [
      {
        "text": 'Waiting on next move by @' + game.next_move_user,
      }
    ]
  })

"""
Show the stats for top scorer
"""
def top_score():
  scores = score_top(serialize=False)
  return jsonify({
    "response_type": "in_channel",
    "text": "Stats",
    "attachments": [
      {
        "text": dict_to_ordered_str(scores)
      }
    ]
  })


def dict_to_ordered_str(d):
  str_rep = ""
  for k, v in sorted(d.iteritems()):
    str_rep += k + ": " + str(v) + "\n"
  return str_rep


def help():
  return "/ttt challenge @another_user [optional board size] to start a new game. /ttt show to show current game. /ttt [1-9] to make a move if it is your turn. The numbers correspond to the 9 squares on a standard 3x3 board. /ttt topscore to see stats about the highest scorer"


"""
Start a new game and cache it in active_games
"""
def setup_game(message, channel, user1, user2, board_size=3):
  if board_size < 1 or board_size > 20:
    return "Let's be reasonable. Valid board_size is [1-20]"
  Game = middleware.add_game_internal(channel=channel, user1=user1, user2=user2,
                                      board_size=board_size, serialize=False)
  active_games[str(channel)] = Game
  board = Board(Game)

  return jsonify({
    "response_type": "in_channel",
    "text": "```" + board.to_ascii() + "```",
    "attachments": [
      {
        "text": "Game setup. Make the first move @" + Game.user1 + ". Valid moves are [1-" + str(
          Game.board_size * Game.board_size) + "]"
      }
    ]
  })


"""
Process a move for currently active game. Persist to DB only when the game finishes
"""
def process_move(message, channel, user):
  pos = int(message)
  current_game = active_games.get(channel)

  # Do not update the db but transition the game with this move
  success, message, board = middleware.update_game_internal(  id=current_game.id,
                                                              user=user,
                                                              pos=pos,
                                                              serialize=False,
                                                              game=current_game,
                                                              updatedb=False)

  # Only persist to DB when the game is finished
  if board.game.state != "ongoing":
    middleware.direct_update_game(board)
    active_games.pop(channel)

  if not success:
    return message
  else:
    return jsonify({
      "response_type": "in_channel",
      "text": "```" + board.to_ascii() + "```",
      "attachments": [
        {
          "text": message
        }
      ]
    })
