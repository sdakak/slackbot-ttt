Summary
=======

Full list of apis is at `/api`
-----------------------------

The main features are:

1. Full CRUD operations for Game which represents a TicTacToe
    * The CRUD API is at `api/game`
    * You can POST with body containing `channel, user1, user2, [optional board_side=3]` to set up a new game. This command will return a unique game ID
    * You can GET `api/game/[id]</id>` to see the current state of the game
    * You can PUT `api/game/[id]` with user, pos to make a move for that user
        * Position can be 1-9 for a regular 3x3 board. In this case it works like a phone keypad. Top left is 1. Bottom right is 9
        * For larger boards say 5x5, and yes you can play arbitrary boards up to 20x20, Pos will be 1-25. Here 5 will be rightmost square in first row. And 6 leftmost square in second row
2. View past game statistics at `api/score/top`. This shows stats like highest scoring player, their win rate, who their most wins were against, how often they made the first move when they won, most winning row/col, etc
3. All data is persisted to a MySQL DB.
    * This allows scaling. Since the computation, IO and network requirements for this program are very small. The only bottleneck is memory. By persisting to DB we can support vastly more traffic than an in-memory store
    * Persisting to DB also allows us to be distributed since the individual service nodes are stateless. We can deploy and change cluster size at will
    * This does reduce the latency but the use case seems to not require absolute speed. And if speed was required we could implement a LRU Cache
    * The DB also allows calculating game stats as shown above
4. Finally, there is a slack_bot controller which parses slash commands and invokes our REST API in return. This way the REST API is completely separate from slack. In the future we can add other clients like slack's super secret human machine interface, mobile apps or browsers

See /about page for more details

Delpoyment
----------
* Create virtualenv and install requirements.txt
* Install mysql server and create a database
* Set two environment variables `JAWSDB_URL=[mysql+mysqldb://user:password@127.0.0.1/db_name]` and `FLASK_SECRET_KEY=arbitrary_string`
* Now we have to make and fill the DB with some toy data:
    * Comment out slack_bot and score_top in routes.py. In data_service_provider.py make sure the initialize and filldb methods don't exit early with a premutare pass
    * Run app.py and hit these apis to create tables and populate it `/api/initdb` and `/api/filldb`
    * Shut down the app, and in data_service_provider.py prematurely end those methods so someone doesn't reinitialize an already initializedb
* Run app.py and point the slack slash command /api/slack/bot