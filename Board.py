"""
This class encapsulates the Game Object and maintains a Matrix representation of the Game.
It contains all of business logic to make the moves on the Matrix and keep the Matrix and Game in sync
"""
class Board:
  def __init__(self, game):
    """
    Stores the Game object and a Matrix representation of the board
    """
    self.game = game
    self.board = self.parse_board_str()

  def parse_board_str(self):
    """
    Create a Matrix represenation of board from string representation inside Game
    :return: Matrix representation of board
    """
    board = [[0 for x in range(self.game.board_size)] for y in
             range(self.game.board_size)]
    i = 0
    for r in range(self.game.board_size):
      for c in range(self.game.board_size):
        board[r][c] = int(self.game.board_str[i])
        i += 1
    return board

  def pos_to_row_column(self, pos):
    """
    Convert posto board indices

    :param pos: number indicating the user entered board position
    :return: tuple (1, 0) representing corresponding board indices
    """
    return (pos - 1) / self.game.board_size, (pos - 1) % self.game.board_size

  def move(self, user, pos):
    """
    Make move at given position for user. This is the heart of the program

    :param user: user making move
    :param pos: position where they are making move
    :return: Tuple containing True or False depending on whether it succeeded and message to User
    """
    # Convert user entered position to board indices
    board_pos = self.pos_to_row_column(pos)

    # Check if the user's move was valid and if not valid return the error message to user
    valid = self.validate(user, board_pos)
    if not valid[0]:
      return (False, valid[1])

    # Set the game.first_move if it's a new game
    if all(c == '0' for c in self.game.board_str):
      self.game.first_move = pos

    # Update the board with the move
    self.update_board(user, board_pos)

    # Update the board state to represent the move just made
    self.update_state()

    # Depending on the state set the win_user if required and pass on a helpful message to the user
    if self.game.state != "ongoing":
      if self.game.win_user is not None:
        return (
          True,
          'Congratulations! @' + self.game.win_user + ' ' + self.game.state)
      return (True, 'Game is a draw! You fought hard!')
    else:
      return (True, "OK. Your move now @" + self.game.next_move_user)

  def update_state(self):
    """
    Update the state of the board to won, draw or ongoing and the winning user

    :return: None
    """
    if self.is_draw():
      self.game.state = "draw"
    won_result = self.is_won()
    if won_result[0]:
      self.game.state = won_result[1]
      self.game.win_user = self.game.user1 if self.game.next_move_user == self.game.user2 else self.game.user2

  def is_won(self):
    """
    Is the board won.

    :return: Tuple Eg. (True, win_row0).
      1st: True if board is won False otherwise
      2nd: win direction or ongoing
    """
    won = False
    # check all rows
    for i in range(self.game.board_size):
      if self.check_row_is_won(i):
        return (True, "won_row" + str(i))

    # check all cols
    for i in range(self.game.board_size):
      if self.check_col_is_won(i):
        return (True, "won_col" + str(i))

    # check both diagonals
    for i in [0, self.game.board_size - 1]:
      if self.check_diagonal_is_won(i):
        return (True, "won_diagonal" + str(i))

    return (False, "ongoing")

  def check_row_is_won(self, row):
    """
    Is given row won

    :param row: to check
    :return: True if given row is won
    """
    for i in range(self.game.board_size - 1):
      if self.board[row][i] == 0:
        return False
      if self.board[row][i] != self.board[row][i + 1]:
        return False
    return True

  def check_col_is_won(self, col):
    """
    Is given col won

    :param col: to check
    :return: True if given col is won
    """
    for i in range(self.game.board_size - 1):
      if self.board[i][col] == 0:
        return False
      if self.board[i][col] != self.board[i + 1][col]:
        return False
    return True

  def check_diagonal_is_won(self, diag):
    """
    Is given diagonal won. A board can only have two diagnals topleft
     represented by 0 or topright represented by board_size-1

    :param diag: to check. Valid values are 0 or board_size-1
    :return: True if given diagonal is won
    """
    # Check diagonal starting top left.
    if diag == 0:
      for i in range(self.game.board_size - 1):
        if self.board[i][i] == 0:
          return False
        if self.board[i][i] != self.board[i + 1][i + 1]:
          return False
    # Check diagonal starting top right
    else:
      for i in range(self.game.board_size - 1):
        if self.board[i][self.game.board_size - 1 - i] == 0:
          return False
        if self.board[i][self.game.board_size - 1 - i] != self.board[i + 1][
                  self.game.board_size - i - 2]:
          return False

    return True

  def is_draw(self):
    """
    Is the game in a draw state?

    :return: If the game is a draw
    """
    for r in range(self.game.board_size):
      for c in range(self.game.board_size):
        if self.board[r][c] == 0:
          return False
    return True

  def update_board(self, user, board_pos):
    """
    Update the board with the given user's move

    :param user: user making the move
    :param board_pos:  position where they are making move
    :return: None
    """
    if user == self.game.user1:
      self.board[board_pos[0]][board_pos[1]] = 1
      self.game.next_move_user = self.game.user2
    else:
      self.board[board_pos[0]][board_pos[1]] = 2
      self.game.next_move_user = self.game.user1
    self.game.board_str = self.board_to_str()

  def board_to_str(self):
    """
    Convert the board matrix to string. Useful when updating the object

    :return: string representing board
    """
    s = ""
    for r in range(self.game.board_size):
      for c in range(self.game.board_size):
        s += str(self.board[r][c])
    return s

  def validate(self, user, board_pos):
    """
    Check if the current move is valid

    :param user: user making the move
    :param board_pos: position where they are making move
    :return: True if move is valid
    """
    if self.game.state != "ongoing":
      return (False,
              'Can only play game in ongoing state. Current state is ' + self.game.state)

    if user != self.game.next_move_user:
      return (False,
              'Can only make move if it is your turn. Current move should be made by @' + self.game.next_move_user)

    if board_pos[0] < 0 or board_pos[0] > self.game.board_size or board_pos[
      1] < 0 or board_pos[1] > self.game.board_size:
      return (False, 'Can only enter positions between 1-' + str(
        self.game.board_size * self.game.board_size))

    if self.board[board_pos[0]][board_pos[1]] != 0:
      return (False,
              'Can only make move in empty space. Try again @' + self.game.next_move_user)

    return (True, 'Valid Move')

  def to_ascii(self):
    """
    Return ASCII reperesntation of board
    The wildness in this method represents the wildness inherent in text formatting

    :return: String representing ascii board representation
    """
    ascii = ''
    s = self.game.board_size
    i = 0

    for r in range(s * 2 - 1):
      for c in range((s * 4) + 1):
        if r % 2 == 0:
          if c in range(0, s * 4 + 1, 4):
            ascii += "|"
          elif c in range(2, s * 4, 4):
            if (self.game.board_str[i] == "1"):
              ascii += "X"
            elif (self.game.board_str[i] == "2"):
              ascii += "O"
            else:
              ascii += str(i + 1) if i < 9 else " "
            i += 1
          else:
            ascii += " "
        else:
          if c in range(0, s * 4 + 1, 4):
            ascii += "|"
          else:
            ascii += "-"
      ascii += "\n"

    return ascii
