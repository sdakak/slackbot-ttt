from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from Models import init_database
from Models.Game import Game

"""
This class abstracts away the specifics of moving data around from DB to our service. This way if we change
our data providers in the future only this class will have to change
"""


class DataProviderService:
  def __init__(self, engine):
    """
    :param engine: The engine route and login details
    :return: a new instance of DAL class
    :type engine: string
    """
    if not engine:
      raise ValueError(
          'The values specified in engine parameter has to be supported by SQLAlchemy')
    self.engine = engine
    db_engine = create_engine(engine)
    db_session = sessionmaker(bind=db_engine)
    self.session = db_session()

  def add_game(self, channel, user1, user2, next_move_user,
      board_size, board_str, state):
    """
    Creates and saves a new Game to the database

    :return: The id of the new game
    """

    new_game = Game(channel=channel,
                    user1=user1,
                    user2=user2,
                    next_move_user=next_move_user,
                    board_size=board_size,
                    board_str=board_str,
                    state=state
                    )

    self.session.add(new_game)
    self.session.commit()

    return new_game

  def get_game(self, id=None, serialize=False):
    """
    If the id parameter is  defined then it looks up the game with the given id,
    otherwise it loads all games

    :param id: The id of the game which needs to be loaded (default value is None)
    :return: The game or all games
    """

    all_games = []

    if id is None:
      all_games = self.session.query(Game).order_by(Game.id).all()
    else:
      all_games = self.session.query(Game).filter(Game.id == id).all()

    if serialize:
      return [g.serialize() for g in all_games]
    else:
      return all_games

  def update_game(self, id, new_game, serialize=True):
    """
    Update the game object in DB with the given one

    :param id: DB object to update
    :param new_game: DB object to update with
    :return: Updated game object
    """
    updated_game = None
    game = self.get_game(id)[0]

    if game:
      self.session.add(new_game)
      self.session.commit()
      updated_game = self.get_game(id)[0]

    if serialize:
      return updated_game.serialize()
    else:
      return updated_game

  def delete_game(self, id):
    """
    Delete a game.

    :param id: Game to delete
    :return: True or False indicated whether the operation was successful or not
    """
    if id:
      candidate_game = self.get_game(id)[0]
      self.session.delete(candidate_game)
      self.session.commit()
      if candidate_game:
        return True
    return False

  def init_database(self):
    """
    Initializes the database tables and relationships
    :return: None
    """
    pass
    init_database(self.engine)

  def fill_database(self):
    """
    Super cool method to fill the DB with some sample games. This

    :return: None. This method is stone cold (Steve Austin)
    """
    pass
    #
    # Games
    #
    g1 = Game(channel="C111",
              user1="U111",
              user2="U222",
              next_move_user="U222",
              win_user="U111",
              first_move=1,
              board_size=3,
              state="won_row0",
              board_str="111212120"
              )
    g2 = Game(channel="C111",
              user1="U111",
              user2="U222",
              next_move_user="U222",
              win_user="U111",
              first_move=2,
              board_size=3,
              state="won_row1",
              board_str="212111212"
              )
    g3 = Game(channel="C111",
              user1="U111",
              user2="U222",
              next_move_user="U222",
              win_user="U111",
              first_move=1,
              board_size=3,
              state="won_row0",
              board_str="111212212"
              )
    g4 = Game(channel="C111",
              user1="U111",
              user2="U222",
              next_move_user="U222",
              win_user="U111",
              first_move=5,
              board_size=3,
              state="won_col1",
              board_str="012212210"
              )
    g5 = Game(channel="C111",
              user1="U111",
              user2="U222",
              next_move_user="U222",
              win_user="U111",
              first_move=1,
              board_size=3,
              state="won_row0",
              board_str="111212200"
              )
    g6 = Game(channel="C111",
              user1="U222",
              user2="U333",
              next_move_user="U222",
              win_user="U222",
              first_move=9,
              board_size=3,
              state="won_col2",
              board_str="021201211"
              )
    g7 = Game(channel="C111",
              user1="U222",
              user2="U333",
              next_move_user="U222",
              win_user="U222",
              first_move=2,
              board_size=3,
              state="won_row1",
              board_str="212111212"
              )
    g8 = Game(channel="C111",
              user1="U222",
              user2="U333",
              next_move_user="U222",
              win_user="U222",
              first_move=1,
              board_size=3,
              state="won_row0",
              board_str="111212212"
              )
    g9 = Game(channel="C111",
              user1="U444",
              user2="U555",
              next_move_user="U444",
              win_user="U555",
              first_move=5,
              board_size=3,
              state="won_row1",
              board_str="012222010"
              )
    g10 = Game(channel="C111",
               user1="U444",
               user2="U555",
               next_move_user="U444",
               win_user="U555",
               first_move=1,
               board_size=3,
               state="won_row2",
               board_str="212010222"
               )

    self.session.add(g1)
    self.session.add(g2)
    self.session.add(g3)
    self.session.add(g4)
    self.session.add(g5)
    self.session.add(g6)
    self.session.add(g7)
    self.session.add(g8)
    self.session.add(g9)
    self.session.add(g10)
    self.session.commit()
