from collections import defaultdict

from flask import jsonify

from middleware import all_games

"""
This method calculates game stats
"""
def score_top(serialize=True):
  games = all_games(serialize=False)

  # populate dict with user -> wins
  most_wins = defaultdict(int)
  for g in games:
    if (g.win_user):
      most_wins[g.win_user] += 1

  # Go through this dict to get user with most wins
  max_user, max_wins = max_value_from_dict(most_wins)

  # Calculate win_rate for top_scorer
  won_games, lost_games = get_win_lost_games(games, max_user)

  # Calculate most wins against
  most_wins_against_dict = defaultdict(int)
  for Game in won_games:
    if (Game.user1 != max_user):
      most_wins_against_dict[Game.user1] += 1
    else:
      most_wins_against_dict[Game.user2] += 1
  most_wins_against, most_won = max_value_from_dict(most_wins_against_dict)

  # Calculate most winning first move
  # WARNING: This doesn't give this user's most winning first move. But the first_move position in all the games this user won
  first_moves_dict = defaultdict(int)
  for Game in won_games:
    first_moves_dict[Game.first_move] += 1
  first_move, first_move_count = max_value_from_dict(first_moves_dict)

  # Calculate percent of winning games where user moved first
  won_when_user_moved_first = 0
  for Game in won_games:
    if Game.user1 == max_user:
      won_when_user_moved_first += 1

  # Calculate max win direction
  win_dir_dict = defaultdict(int)
  for Game in won_games:
    win_dir_dict[Game.state] += 1
  win_dir, win_dir_count = max_value_from_dict(win_dir_dict)

  stats_dict = {
    "best_score_user": max_user,
    "best_score_win_games_count": len(won_games),
    "best_score_win_rate_percent": len(won_games) * 100 / (
      float(len(won_games)) + len(lost_games)),
    "most_wins_against_player": most_wins_against,
    "most_wins_against_player_count": most_won,
    "most_wins_against_player_percent": most_won * 100 / (
      float(len(won_games)) + len(lost_games)),
    "most_frequent_first_move_pos_in_wins": first_move,
    "most_frequent_first_move_pos_in_wins_count": first_move_count,
    "most_frequent_first_move_pos_in_wins_percent": first_move_count * 100 / float(
        len(won_games)),
    "won_when_this_user_moved_first_count": won_when_user_moved_first,
    "won_when_this_user_moved_first_percent": won_when_user_moved_first * 100 / float(
        len(won_games)),
    "most_winning_state": win_dir,
    "most_winning_state_count": win_dir_count,
    "most_winning_state_percent": win_dir_count * 100 / float(len(won_games))
  }

  if serialize:
    return jsonify(stats_dict)
  else:
    return stats_dict


"""
Return the key, value that have the max value from a dict
"""
def max_value_from_dict(d):
  mk = None
  mv = -1
  for k, v in d.iteritems():
    if v > mv:
      mk = k
      mv = v

  return mk, mv


"""
Separate a list of games into games won and games lost
"""
def get_win_lost_games(games, user):
  won_games = []
  lost_games = []
  for Game in games:
    if Game.state != "ongoing" and Game.state != "draw":
      if Game.win_user == user:
        won_games.append(Game)
      elif Game.user1 == user or Game.user2 == user:
        lost_games.append(Game)
  return won_games, lost_games
