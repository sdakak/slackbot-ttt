import os

from flask import abort
from flask import jsonify
from flask import make_response
from flask import request
from flask import url_for

from Board import Board
from data_provider_service import DataProviderService

db_engine = os.environ['JAWSDB_URL']
DATA_PROVIDER = DataProviderService(db_engine)

"""
This class is the intelligent portion of the service. 

It has the add_game method that starts off a new game with the correct game state.
And the update_game method which makes a move on an ongoing game and then persists it.

All the methods call the data_provider_service methods to get the data from DB and then 
return either JSONs or objects depending on what was asked.
"""


def game_by_id(id, serialize=True):
  current_game = DATA_PROVIDER.get_game(id, serialize=serialize)
  if current_game:
    if serialize:
      return jsonify({"game": current_game})
    else:
      return True, current_game[0]
  elif serialize:
    #
    # In case we did not find the game
    # we send HTTP 404 - Not Found error to the client
    #
    abort(404)
  else:
    return False, "Game Not Found"


def delete_game(id):
  if DATA_PROVIDER.delete_game(id):
    return make_response('', 200)
  else:
    return abort(404)


def all_games(serialize=True, state=None):
  all_games = DATA_PROVIDER.get_game(serialize=serialize)
  if serialize:
    return jsonify({"games": all_games, "total": len(all_games)})
  else:
    return all_games


"""
  Get the given game from DB and update it with the move passed in.
  It returns updated game state, message to the user and ASCII representation of the board.
  The message also returns HTTP Status codes and custom messages to the user.
"""


def update_game(id):
  user = request.form["user"]
  pos = request.form["pos"]
  return update_game_internal(id, user, pos)


def update_game_internal(id, user, pos, serialize=True, game=None, updatedb=True):
  if game is None:
    current_game = game_by_id(id, False)[1]
  else:
    current_game = game
  board = Board(current_game)
  move_resp = board.move(user, int(pos))
  if move_resp[0] is False and serialize:
    abort(400, move_resp[1])

  updated_game = None
  if updatedb:
    updated_game = direct_update_game(board)
  if serialize:
    return jsonify({
      "game": updated_game,
      "message": move_resp[1],
      "ascii": board.to_ascii()
    })
  else:
    return move_resp[0], move_resp[1], board

def direct_update_game(board):
  updated_game = DATA_PROVIDER.update_game(board.game.id, board.game, True)
  if not updated_game:
    abort(404)
  return updated_game


"""
  Starts a new game between the given users and persists it to the DB.
  The main work here is to put the game in the correct initial state.
"""


def add_game():
  channel = request.form["channel"]
  user1 = request.form["user1"]
  user2 = request.form["user2"]
  req_board_size = request.form.get("board_size")
  board_size = int(req_board_size) if req_board_size is not None else 3

  return add_game_internal(channel, user1, user2, board_size)


def add_game_internal(channel, user1, user2, board_size=3, serialize=True):
  next_move_user = user1
  state = "ongoing"
  board_str = "0" * (board_size * board_size)

  new_game = DATA_PROVIDER.add_game(channel=channel,
                                    user1=user1,
                                    user2=user2,
                                    next_move_user=next_move_user,
                                    board_size=board_size,
                                    board_str=board_str,
                                    state=state
                                    )

  if serialize:
    return jsonify({
      "id": new_game.id,
      "url": url_for("game_by_id", id=new_game.id)
    })
  else:
    return new_game


def build_message(key, message):
  return {key: message}


def initialize_database():
  DATA_PROVIDER.init_database()


def fill_database():
  DATA_PROVIDER.fill_database()
