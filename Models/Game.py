from sqlalchemy import Column, String, Integer

from Model import Model


class Game(Model):
  __tablename__ = 'game'
  id = Column(Integer, primary_key=True, nullable=False, autoincrement=True)
  channel = Column(String(100), nullable=False)
  user1 = Column(String(100), nullable=False)
  user2 = Column(String(100), nullable=False)
  next_move_user = Column(String(100), nullable=False)
  win_user = Column(String(100), nullable=True)
  first_move = Column(Integer, nullable=True)
  board_size = Column(Integer, nullable=True)
  state = Column(String(100), nullable=True)
  # possible values for state:
  # won_row0, won_row1, won_row2, won_col0, won_col1, won_col2, won_diagonal0, won_diagonal2, draw, ongoing, corrupt
  board_str = Column(String(500), nullable=True)

  #
  # METHODS
  #

  def serialize(self):
    return {
      "id": self.id,
      "channel": self.channel,
      "user1": self.user1,
      "user2": self.user2,
      "next_move_user": self.next_move_user,
      "first_move": self.first_move,
      "state": self.state,
      "win_user": self.win_user,
      "board": self.board_str
    }

  def __str__(self):
    return "id: %s, channel: %s, user1: %s, user2: %s, next_move_user: %s, first_move: %s, state: %s, win_user: %s, board_size: %s, state: %s, board: %s" % (
      self.id, self.channel, self.user1, self.user2, self.next_move_user,
      self.first_move, self.state, self.win_user, self.board_str, self.state,
      self.board_str)

  def __eq__(self, other):
    return self.id == other.id

  def __gt__(self, other):
    return self.id > other.id
